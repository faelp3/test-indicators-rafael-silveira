import { Component, OnInit } from '@angular/core';
import { Country } from '../core/models/country';
import { Indicator } from '../core/models/indicator';
import { CountryService } from '../core/services/country-service.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  
  selectedCountry: string;

  countriesCode: Country[];

  indices: Indicator[];

  page: number;

  constructor(private countryService: CountryService) { }

  ngOnInit(): void {
    this.countryService.getCountrysAvailable().subscribe(
      countries => {
        console.log('getCountrysAvailable:' + JSON.stringify(countries));
        this.countriesCode = countries;
      },error => {
        console.error("cannot find countries" + error);
      }
    );
  }

  search(event:any):void {
    this.countryService.getIndicatorsCountrysAvailable(this.selectedCountry,this.page).subscribe(
      indicesResult => {
        console.log('getIndicatorsCountrysAvailable:' + JSON.stringify(indicesResult));
        this.indices = indicesResult;
      },error => {
        console.error("cannot find indicators" + error);
      }
    );
  }

}
