import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Country } from '../models/country';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private http: HttpClient) { }

  URL_API = `${environment.BASE_URL}/country`;
  

  getCountrysAvailable(): Observable<Country[]> {
    return this.http.get<Country[]>(this.URL_API);
  }

  private handlePageNumber(page: number): string {
    if (page === null || page === undefined || page === 0)
      page = 1;
    return String(page);
  }

  getIndicatorsCountrysAvailable(code: string, page:number): Observable<any> {
    const params = new HttpParams().set('page', this.handlePageNumber(page));
    return this.http.get<any[]>(`${this.URL_API}/${code}/indicators`, {params: params});
  }
}
