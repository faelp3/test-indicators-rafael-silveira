export class Indicator {
    id: string;
    value: string;

    constructor(id?: string, value?: string) {
        this.id = id || '';
        this.value = value || '';
    }
}