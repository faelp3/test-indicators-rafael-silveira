import { Country } from "./country";
import { Indicator } from "./indicator";

export class Indice {
    indicator: Indicator
    country: Country
    countryiso3code: string
    date: string
    value: any
    unit: string
    obs_status: string
    decimal: number
}