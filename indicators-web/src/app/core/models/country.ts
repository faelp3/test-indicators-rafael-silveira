export class Country {
    id: string;
    value: string;

    constructor(id?: string, value?: string) {
        this.id = id || '';
        this.value = value || '';
    }
}