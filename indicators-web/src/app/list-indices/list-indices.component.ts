import { Component, Input, OnInit } from '@angular/core';
import { Indice } from '../core/models/indice';
import { CountryService } from '../core/services/country-service.service';

@Component({
  selector: 'app-list-indices',
  templateUrl: './list-indices.component.html',
  styleUrls: ['./list-indices.component.scss']
})
export class ListIndicesComponent implements OnInit {

  // indices: Indice[] = [];
  @Input() indices: Indice[];

  constructor(private countryService: CountryService) { }

  ngOnInit(): void {
    
    // let j = Math.floor(Math.random() * 40);
    // for (let i = 0; i < j; i++) {
    //   const fakeIndice = new Indice();
    //   fakeIndice.country = new Country('BR', 'Brasil');
    //   fakeIndice.indicator = new Indicator('some', 'blah');
    //   fakeIndice.date = '' + (2020 + i);
    //   fakeIndice.unit = 'UN'
    //   fakeIndice.value = Math.floor(Math.random() * 10);
    //   this.indices.push(fakeIndice);
    // }
  }

  search(selected: any): void{
    console.log("chamou no filho: " + selected);
   
  }

}
