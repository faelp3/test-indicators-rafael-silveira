import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Country } from '../core/models/country';
import { CountryService } from '../core/services/country-service.service';

@Component({
  selector: 'app-country-form',
  templateUrl: './country-form.component.html',
  styleUrls: ['./country-form.component.scss']
})
export class CountryFormComponent implements OnInit {

  @Input() countriesCode: Country[];
  @Output() searchEvent = new EventEmitter<string>();

  @Input() selectedCountry: string;

  constructor() { }

  ngOnInit(): void {

  }

  search() {
    this.searchEvent.emit(this.selectedCountry)
  }

}
