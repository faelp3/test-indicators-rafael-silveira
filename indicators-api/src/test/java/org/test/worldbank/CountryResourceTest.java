package org.test.worldbank;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class CountryResourceTest {

    private final String GET_COUNTRY = "/country";

    private final String JSON_INDICATORS_COUNTRY_BR = "{\"page\":1,\"pages\":2,\"perPage\":0,\"total\":0,\"lastupdated\":\"2021-06-26\",\"indicators\":[{\"date\":\"2021\"}]}";

    private final String JSON_COUNTRIES = "{[\"id\":\"BR\",\"pages\":\"BRAZIL\",[\"id\":\"PT\",\"value\":\"PORTUGAL\"]}";

    @Test
    public void testGetIndicatorsCountry() {
        given()
          .when().get(GET_COUNTRY.concat("/BR/indicators"))
          .then()
             .statusCode(200)
             .body(is(JSON_INDICATORS_COUNTRY_BR));
    }

    @Test
    public void testGetCountries() {
        given()
          .when().get(GET_COUNTRY)
          .then()
             .statusCode(200)
             .body(is(JSON_COUNTRIES));
    }

}