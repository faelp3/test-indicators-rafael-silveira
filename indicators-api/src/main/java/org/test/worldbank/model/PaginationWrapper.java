package org.test.worldbank.model;

import java.time.LocalDate;
import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaginationWrapper {

    private int page;
    private int pages;
    
    @JsonAlias("per_page")
    private int perPage;

    private int total;
    private String sourceid;
    private String sourcename;
    private LocalDate lastupdated;

    private Collection<IndicatorCountry> indicators;

    private Collection<Country> countries;

    
    public Collection<IndicatorCountry> getIndicators() {
        return this.indicators;
    }

    public void setIndicators(Collection<IndicatorCountry> indicators) {
        this.indicators = indicators;
    }

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPages() {
        return this.pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getPerPage() {
        return this.perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public int getTotal() {
        return this.total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getSourceid() {
        return this.sourceid;
    }

    public void setSourceid(String sourceid) {
        this.sourceid = sourceid;
    }

    public String getSourcename() {
        return this.sourcename;
    }

    public void setSourcename(String sourcename) {
        this.sourcename = sourcename;
    }

    public LocalDate getLastupdated() {
        return this.lastupdated;
    }

    public void setLastupdated(LocalDate lastupdated) {
        this.lastupdated = lastupdated;
    }

    public Collection<Country> getCountries() {
        return countries;
    }

    public void setCountries(Collection<Country> countries) {
        this.countries = countries;
    }

}
