package org.test.worldbank.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.test.worldbank.model.PaginationWrapper;

@Path("/v2/country")
@RegisterRestClient(configKey="worldbank-api")
public interface CountryServiceWorldBank {

    @GET
    PaginationWrapper getCountriesPaginationWrapper(@QueryParam("format") String format, @QueryParam("page") int page);

    @GET
    @Path("/{code}/SI.POV.DDAY")
    PaginationWrapper getIndicatorsBy(@PathParam("code") String code, @QueryParam("format") String format, @QueryParam("page") int page);

}

