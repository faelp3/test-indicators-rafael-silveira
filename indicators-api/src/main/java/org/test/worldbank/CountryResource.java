package org.test.worldbank;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.test.worldbank.model.Country;
import org.test.worldbank.model.PaginationWrapper;
import org.test.worldbank.service.CountryServiceWorldBank;

@Path("/api/v1/country")
public class CountryResource {

    @Inject
    @RestClient
    CountryServiceWorldBank service;

    final String format = "json";

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Country> getList() {
        Set<Country> list = new HashSet<>();
        int page = 1;
        boolean fill = true;
        while (fill){
            PaginationWrapper countries = service.getCountriesPaginationWrapper(format, page);
            list.addAll(countries.getCountries());
            if (countries == null || countries.getPage() == page)
                fill = false;
            page++;
        }
        return list;
    }
    
    @GET
    @Path("/{code}/indicators")
    @Produces(MediaType.APPLICATION_JSON)
    public PaginationWrapper getIndicators(String code, @QueryParam("page") int page) {
        return service.getIndicatorsBy(code, format, page);
    }

}